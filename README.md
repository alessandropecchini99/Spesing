<img src="static/img/Banner-Spesing-Light.png" width="250">

[![GitHub release (latest by date including pre-releases)](https://img.shields.io/github/v/release/navendu-pottekkat/awesome-readme?include_prereleases)](https://img.shields.io/github/v/release/navendu-pottekkat/awesome-readme?include_prereleases)
[![GitHub last commit](https://img.shields.io/github/last-commit/navendu-pottekkat/awesome-readme)](https://img.shields.io/github/last-commit/navendu-pottekkat/awesome-readme)

Il nostro gestore di spese personali!
 
# Installation

Queste sono le istruzioni per il setup del progetto

**PREREQUISITI:**
- python 3.12.1 - ``` python --version ```
- pipenv - ``` pipenv --version ```

Una volta controllato di avere il necessario, questi sono i comandi in ordine di esecuzione:

**1 -** In un terminale entrare nella cartella dove si vuole installare il progetto

**2 -** Eseguire il seguente comando per clonare il progetto dalla repository:
``` git clone https://codeberg.org/alessandropecchini99/Spesing.git ```

**3 -** Entrare nella cartella del progetto
``` cd Spesing ```

**4 -** Attivare l'ambiente di sviluppo:
``` python -m pipenv shell ```

**5 -** Installare i requisiti:
``` pip install -r requirements.txt ```

**6 -** Impostare le migrazioni:
``` python manage.py makemigrations ```

**7 -** Effettuare le migrazioni:
``` python manage.py migrate ```

**8 -** Caricare i dati demo nel progetto:

``` python manage.py loaddata project_data.json ```

**9 -** Avviare il server:
``` python manage.py runserver ```

-----------------------------------------

 
# Usage

Nei dati Demo, sono presenti diversi account di prova con le seguenti credenziali:

**STAFF USER**

Utente con i permessi necessari ad accedere alla zona Admin
- username: admin
- password: admindjango123

**NORMAL USER**

Utenti con associate diverse spese

- Gli username disponibili sono:

pek

roby

sevy

fmammi

- Mentre le rispettive password sono:

pekdjango123

robydjango123

sevydjango123

fmammidjango123

