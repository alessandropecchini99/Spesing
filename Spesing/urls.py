"""
URL configuration for Spesing project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from .views import HomePageView, AboutPageView, custom_404, custom_500
from users import urls as user_urls
from spese import urls as spese_urls


# Custom Errors
handler404 = 'Spesing.views.custom_404'
handler500 = 'Spesing.views.custom_500'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', HomePageView.as_view(), name='homepage'),
    path('about/', AboutPageView.as_view(), name='about'),
    path('user/', include(user_urls), name='users'),
    path('spese/', include(spese_urls), name='spese'),
    # Da togliere quando si finisce di testare e di fare il layout
    path('error404/', custom_404, name='custom_error_404'),
    path('error500/', custom_500, name='custom_error_500')
]
