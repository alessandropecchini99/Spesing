from django.shortcuts import render
from django.views.generic import TemplateView
from spese.models import Spesa, Categoria

__all__ = ["custom_500",
           "custom_404",
           "AboutPageView",
           "HomePageView"
           ]


class HomePageView(TemplateView):
    template_name = "homepage.html"

    def get_context_data(self, **kwargs):
        # Calcolo tutte le spese totali non filtrate per user
        all_spese = Spesa.objects.all()
        n_spese_totali = len(all_spese)
        # Calcolo l'importo totale speso non filtrato per user
        totale_importo_spese = 0
        for spesa in all_spese:
            totale_importo_spese += spesa.importo
        return {"n_spese_totali": n_spese_totali,
                "totale_importo_spese": totale_importo_spese}


class AboutPageView(TemplateView):
    template_name = "about.html"


def custom_404(request, *args, **argv):
    return render(request, 'errori/404.html', status=404)


def custom_500(request, *args, **argv):
    return render(request, 'errori/500.html', status=500)
