from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Div, Button, Submit, HTML
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm


class UserForm(UserCreationForm):
    password1 = forms.CharField(
        label="Password",
        widget=forms.PasswordInput,
        strip=False,
    )
    password2 = forms.CharField(
        label="Password confirmation",
        widget=forms.PasswordInput,
        strip=False,
    )

    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'username',
            'email',
            'password1',
            'password2'
        )
        help_texts = {'username': None, 'email': None, 'password1': None, 'password2': None}

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
        self.helper.layout = Layout(
            Div(
                Div(
                    Div(
                        Field(
                            'first_name', css_class='form-control', placeholder='Nome'),
                        css_class='form-floating mb-3'),
                    css_class='col-12 col-lg-6'),
                Div(
                    Div(
                        Field(
                            'last_name', css_class='form-control', placeholder='Cognome'),
                        css_class='form-floating mb-3'),
                    css_class='col-12 col-lg-6'),
                Div(
                    Div(
                        Field(
                            'username', css_class='form-control', placeholder='Username*'),
                        css_class='form-floating mb-3'),
                    css_class='col-12 col-lg-6'),
                Div(
                    Div(
                        Field(
                            'email', css_class='form-control', placeholder='Email'),
                        css_class='form-floating mb-3'),
                    css_class='col-12 col-lg-6'),
                Div(
                    Div(
                        Field(
                            'password1', css_class='form-control', placeholder='Password*'),
                        css_class='form-floating mb-3'),
                    css_class='col-12 col-lg-6'),
                Div(
                    Div(
                        Field(
                            'password2', css_class='form-control', placeholder='PW Confirm*'),
                        css_class='form-floating mb-3'),
                    css_class='col-12 col-lg-6'),
                Div(
                    Div(
                        Submit(
                            css_class='btn btn-primary btn-lg', value='Register', name='login'),
                        css_class='d-grid my-3'),
                    css_class='col-12'),
                Div(HTML('<p class="m-0 text-secondary text-center">Hai già un account? <a href="{% url '
                         '"users:login" %}"'
                         'class="link-primary text-decoration-none">Accedi</a></p>'),
                    css_class='col-12'),
                css_class='row gy-2 overflow-hidden'),
        )


class CustomAuthenticationForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(CustomAuthenticationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
        self.helper.layout = Layout(
            Div(
                Div(
                    Div(
                        Field(
                            'username', css_class='form-control', placeholder='Username'),
                        css_class='form-floating mb-3'),
                    css_class='col-12'),
                Div(
                    Div(
                        Field(
                            'password', css_class='form-control', placeholder='Password'),
                        css_class='form-floating mb-3'),
                    css_class='col-12'),
                Div(
                    Div(
                        Submit(
                            css_class='btn btn-primary btn-lg', value='Log in', name='login'),
                        css_class='d-grid my-3'),
                    css_class='col-12'),
                Div(HTML('<p class="m-0 text-secondary text-center">Non hai un account? <a href="{% url '
                         '"users:register" %}"'
                         'class="link-primary text-decoration-none">Registrati</a></p>'),
                    css_class='col-12'),
                css_class='row gy-2 overflow-hidden'),
        )
