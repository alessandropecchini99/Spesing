from django.contrib.auth.views import LoginView, LogoutView
from django.urls import reverse_lazy
from django.views.generic import CreateView
from users.forms import UserForm, CustomAuthenticationForm

# Create your views here.
__all__ = ["RegisterCreateView",
           "LoginAuthView",
           "LogoutAuthView"
           ]


class RegisterCreateView(CreateView):
    form_class = UserForm
    template_name = "users/register.html"
    success_url = reverse_lazy("spese:home")


class LoginAuthView(LoginView):
    form_class = CustomAuthenticationForm
    template_name = "users/login.html"


class LogoutAuthView(LogoutView):
    pass

