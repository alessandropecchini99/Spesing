from django.urls import path
from . import views

app_name = "users"

urlpatterns = [
    path('register/', views.RegisterCreateView.as_view(), name='register'),
    path('login/', views.LoginAuthView.as_view(), name='login'),
    path('logout/', views.LogoutAuthView.as_view(), name='logout'),
]