function setNewFont() {
    if (document.body.style.fontFamily === "Arial") {
        document.body.style.fontFamily = "'OpenDyslexic', sans-serif";
        localStorage.setItem("fontDyslexic", "true");
        changeIconAccessibility("true");
    } else {
        document.body.style.fontFamily = "Arial";
        localStorage.setItem("fontDyslexic", "false");
        changeIconAccessibility("false");
    }
}

function changeIconAccessibility(statoAccessibility) {
    const accessibilityToggleButton = document.getElementsByClassName('accessibilityModeToggle');
    const accessibilityIcon = document.getElementsByClassName("accessibilityIcon");
    if (statoAccessibility === "true") {
        for (const accessibilityButtonElement of accessibilityToggleButton) {
            accessibilityButtonElement.classList.add("btn-dark");
            accessibilityButtonElement.classList.remove("btn-light");
        }
        for (const accessibilityIconElement of accessibilityIcon) {
            accessibilityIconElement.classList.add('bi-universal-access-circle');
            accessibilityIconElement.classList.remove('bi-universal-access');
        }
    } else {
        for (const accessibilityButtonElement of accessibilityToggleButton) {
            accessibilityButtonElement.classList.add("btn-light");
            accessibilityButtonElement.classList.remove("btn-dark");
        }
        for (const accessibilityIconElement of accessibilityIcon) {
            accessibilityIconElement.classList.add('bi-universal-access');
            accessibilityIconElement.classList.remove('bi-universal-access-circle');
        }
    }
}

document.addEventListener('DOMContentLoaded', function () {
    const isFontActive = localStorage.getItem("fontDyslexic");
    if (isFontActive === "true") {
        document.body.style.fontFamily = "'OpenDyslexic', sans-serif";
        changeIconAccessibility("true");
    } else {
        document.body.style.fontFamily = "Arial";
        changeIconAccessibility("false");
    }
});