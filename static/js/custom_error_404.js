function changeError() {
    const immagini = document.getElementsByClassName("img-404-not-selected");
    let immagineScelta;
    const nRandom = Math.floor((Math.random() * 4) + 1);
    if (nRandom === 1) {
        immagineScelta = immagini.item(0); //fmammi
    } else if (nRandom === 2) {
        immagineScelta = immagini.item(1); //pek
    } else if (nRandom === 3) {
        immagineScelta = immagini.item(2); //roby
    } else {
        immagineScelta = immagini.item(3); //sevy
    }
    immagineScelta.classList.remove("img-404-not-selected");
    immagineScelta.classList.add("img-404-selected");
}

document.addEventListener('DOMContentLoaded', changeError);