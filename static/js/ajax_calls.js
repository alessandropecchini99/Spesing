const getIpInfo = async () => {
    await fetch(`https://api.ipify.org?format=json`)
        .then(response => {
            if (!response.ok) {
                if (response.status === 404) {
                    window.alert(`Error 404: IP NOT FOUND`);
                    throw new Error(`Error: ${response.status}`);
                } else {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }
            }
            return response.json()
        })
        .then(data => {
            document.getElementById("ipaddress").innerHTML = data.ip;
        })
}

const getWeatherInfo = async () => {
    await fetch(`https://api.openweathermap.org/data/2.5/weather?q=modena,it&lang=it&lat=44.6488366&lon=10.9200867&units=metric&appid=f83eb6aeaef497a1677ef574cf515d5c`)
        .then(response => {
            if (!response.ok) {
                if (response.status === 404) {
                    window.alert(`Error 404: WEATHER NOT FOUND`);
                    throw new Error(`Error: ${response.status}`);
                } else {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }
            }
            return response.json()
        })
        .then(data => {
            let nome = data.name;
            let gradi = Math.floor(data.main.temp);
            let stringaMeteo = `${nome} ${gradi}°`;
            document.getElementById("weather").innerHTML = stringaMeteo;
        })
}

document.addEventListener('DOMContentLoaded', getWeatherInfo);