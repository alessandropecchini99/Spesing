let styleText = document.getElementById('base_css').sheet;
let dark = `.inverted {
    filter: invert(1);
}`;
let light = `.inverted {
    filter: invert(0);
}`;

function toggleDarkMode() {
    const darkModeToggleHTML = document.documentElement.getAttribute('data-bs-theme');
    if (darkModeToggleHTML === 'dark') {
        document.documentElement.setAttribute('data-bs-theme','light');
        localStorage.setItem("theme", "light");
        // salva la darkmode in uno store locale che vale anche quando si chiude la pagina (tipo cookies)
        changeIconDarkMode("light");
        styleText.deleteRule(0);
        styleText.insertRule(light, 0);
    } else {
        document.documentElement.setAttribute('data-bs-theme','dark');
        localStorage.setItem("theme", "dark");
        changeIconDarkMode("dark");
        styleText.deleteRule(0);
        styleText.insertRule(dark, 0);
    }
}

function changeIconDarkMode(statoDarkMode) {
    const darkModeToggleButton = document.getElementsByClassName('darkModeToggle');
    const darkModeIcon = document.getElementsByClassName("darkModeIcon");
    if (statoDarkMode === "dark") {
        for (const darkModeButtonElement of darkModeToggleButton) {
            darkModeButtonElement.classList.add("btn-light");
            darkModeButtonElement.classList.remove("btn-dark");
        }
        for (const darkModeIconElement of darkModeIcon) {
            darkModeIconElement.classList.add('bi-brightness-high-fill');
            darkModeIconElement.classList.remove('bi-moon-fill');
        }
    } else {
        for (const darkModeButtonElement of darkModeToggleButton) {
            darkModeButtonElement.classList.add("btn-dark");
            darkModeButtonElement.classList.remove("btn-light");
        }
        for (const darkModeIconElement of darkModeIcon) {
            darkModeIconElement.classList.add('bi-moon-fill');
            darkModeIconElement.classList.remove('bi-brightness-high-fill');
        }
    }
}
document.addEventListener('DOMContentLoaded', function() {
    const theme = localStorage.getItem("theme");
    if (theme === "dark") {
        document.documentElement.setAttribute('data-bs-theme','dark');
        changeIconDarkMode("dark");
        styleText.deleteRule(0);
        styleText.insertRule(dark, 0);
    } else {
        document.documentElement.setAttribute('data-bs-theme','light');
        changeIconDarkMode("light");
        styleText.deleteRule(0);
        styleText.insertRule(light, 0);
    }
});