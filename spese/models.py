import uuid
from datetime import datetime

from django.db import models
from django.contrib.auth.models import User

# Create your models here.

__all__ = ["Abbonamento", 'MacroCategoria', 'Categoria', 'Spesa']


class Abbonamento(models.Model):
    tipologia = models.CharField(max_length=40)

    class Meta:
        verbose_name = 'Abbonamento'
        verbose_name_plural = 'Abbonamenti'

    def __str__(self):
        return self.tipologia


class MacroCategoria(models.Model):
    titolo = models.CharField(max_length=40)
    descrizione = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name = 'Macro Categoria'
        verbose_name_plural = 'Macro Categorie'

    def __str__(self):
        return self.titolo


class Categoria(models.Model):
    macro = models.ForeignKey(MacroCategoria, on_delete=models.CASCADE, default='')
    nome = models.CharField(max_length=40)
    descrizione = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorie'

    def __str__(self):
        return self.nome


class Spesa(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    proprietario = models.ForeignKey(User, on_delete=models.CASCADE, default='')
    nome = models.CharField(max_length=50)
    importo = models.DecimalField(max_digits=8, decimal_places=2)
    data_acquisto = models.DateField()
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE, default='', null=True, blank=True)
    abbonamento = models.ForeignKey(Abbonamento, on_delete=models.CASCADE, default='')
    descrizione = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name = 'Spesa'
        verbose_name_plural = 'Spese'

    def __str__(self):
        return self.nome

    def totale_spese(self):
        spese_utente = Spesa.objects.filter(proprietario=self.proprietario)
        totale = 0
        if len(spese_utente) > 0:
            for spesa in spese_utente:
                totale += spesa.importo
            totale = str(round(totale, 2)) + "€"
            return totale
        else:
            return str(totale)

    def totale_spese_mensile(self):
        spese_utente_mensile = Spesa.objects.filter(proprietario=self.proprietario, abbonamento__tipologia="Mensile")
        totale = 0
        if len(spese_utente_mensile) > 0:
            for spesa in spese_utente_mensile:
                totale += spesa.importo
            totale = str(round(totale, 2)) + "€"
        else:
            totale = str(totale) + "€"
        return totale

    def totale_spese_annuale(self):
        spese_utente_annuale = Spesa.objects.filter(proprietario=self.proprietario, abbonamento__tipologia="Annuale")
        totale = 0
        if len(spese_utente_annuale) > 0:
            for spesa in spese_utente_annuale:
                totale += spesa.importo
            totale = str(round(totale, 2)) + "€"
        else:
            totale = str(totale) + "€"
        return totale

    def n_spese(self):
        spese_utente = Spesa.objects.filter(proprietario=self.proprietario)
        n_spese_utente = len(spese_utente)
        return n_spese_utente
