from django.urls import path
from . import views

app_name = 'spese'

urlpatterns = [
    path('home', views.HomeSpese.as_view(), name='home'),
    path('macrocategorie-list/', views.MacroCategorieListView.as_view(), name='macro-list'),
    path('categorie-list/<int:macro_id>/', views.CategorieListView.as_view(), name='categorie-list'),
    path('list/<int:categoria_id>', views.SpeseListView.as_view(), name='list'),
    path('list/', views.AllSpeseListView.as_view(), name='all-list'),
    path('create/', views.SpesaCreateView.as_view(), name='create'),
    path('delete/<pk>', views.SpesaDeleteView.as_view(), name='delete'),
    path('details/<pk>', views.SpesaDetailView.as_view(), name='details'),
    path('update/<pk>', views.SpesaUpdateView.as_view(), name='update'),
]
