from django.contrib import admin

# Register your models here.
from spese.models import Abbonamento, MacroCategoria, Categoria, Spesa

admin.site.register(Abbonamento)

admin.site.register(MacroCategoria)


class CategoriaAdmin(admin.ModelAdmin):
    list_display = ['nome', 'descrizione']


admin.site.register(Categoria, CategoriaAdmin)


class SpesaAdmin(admin.ModelAdmin):
    list_display = ['nome', 'proprietario', 'importo_euro', 'categoria_nome', 'abbonamento']

    def importo_euro(self, obj):
        return f"{obj.importo}€"

    def categoria_nome(self, obj):
        if obj.categoria is not None:
            return obj.categoria.nome
        else:
            return "Non Definito"


admin.site.register(Spesa, SpesaAdmin)
