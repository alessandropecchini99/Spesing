# Generated by Django 5.0.4 on 2024-04-28 21:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('spese', '0004_alter_spesa_importo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='spesa',
            name='data_acquisto',
            field=models.DateField(),
        ),
    ]
