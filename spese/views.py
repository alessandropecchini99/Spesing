import uuid
from django.contrib.auth.decorators import login_required
from django.forms import DateInput
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, UpdateView, CreateView, DeleteView
from spese.models import MacroCategoria, Categoria, Spesa

# Create your views here.
__all__ = ["MacroCategorieListView",
           "CategorieListView",
           "SpeseListView",
           "AllSpeseListView",
           "SpesaDetailView",
           "SpesaUpdateView",
           "SpesaCreateView",
           "SpesaDeleteView"
           ]


@method_decorator(login_required, name="dispatch")
class HomeSpese(ListView):
    model = Spesa
    template_name = "spese/home_spese.html"

    def get_queryset(self):
        return super().get_queryset().filter(proprietario=self.request.user)


@method_decorator(login_required, name='dispatch')
class MacroCategorieListView(ListView):
    model = MacroCategoria
    template_name = 'spese/macro_list.html'


@method_decorator(login_required, name='dispatch')
class CategorieListView(ListView):
    model = Categoria
    template_name = 'spese/categorie_list.html'

    def get_queryset(self):
        return super().get_queryset().filter(macro_id=self.kwargs['macro_id'])


@method_decorator(login_required, name='dispatch')
class SpeseListView(ListView):
    paginate_by = 5
    model = Spesa
    template_name = 'spese/spese_list.html'

    def get_queryset(self):
        return super().get_queryset().filter(proprietario=self.request.user).filter(
            categoria_id=self.kwargs['categoria_id'])


@method_decorator(login_required, name='dispatch')
class AllSpeseListView(ListView):
    paginate_by = 5
    model = Spesa
    template_name = 'spese/spese_list.html'

    def get_queryset(self):
        queryset = super().get_queryset().filter(proprietario=self.request.user)
        filtro_data = self.request.GET.get('filtro_data', None)
        if filtro_data:
            order_mapping = {
                'decrescente': '-data_acquisto',
                'crescente': 'data_acquisto',
            }
            order_by = order_mapping.get(filtro_data, 'data_acquisto')
            queryset = queryset.order_by(order_by)

        filtro_importo = self.request.GET.get('filtro_importo', None)
        if filtro_importo:
            order_mapping = {
                'decrescente': '-importo',
                'crescente': 'importo',
            }
            order_by = order_mapping.get(filtro_importo, 'importo')
            queryset = queryset.order_by(order_by)
        return queryset


@method_decorator(login_required, name='dispatch')
class SpesaDetailView(DetailView):
    model = Spesa


@method_decorator(login_required, name='dispatch')
class SpesaUpdateView(UpdateView):
    model = Spesa
    fields = ["nome", "importo", "data_acquisto", "categoria", "abbonamento", "descrizione"]
    template_name_suffix = "_update_form"

    def get_success_url(self):
        return reverse_lazy("spese:all-list")


@method_decorator(login_required, name='dispatch')
class SpesaCreateView(CreateView):
    model = Spesa
    fields = ["nome", "importo", "data_acquisto", "categoria", "abbonamento", "descrizione"]
    template_name_suffix = "_create_form"
    success_url = reverse_lazy("spese:all-list")

    def form_valid(self, form):
        form.instance.id = uuid.uuid4()
        form.instance.proprietario = self.request.user
        return super().form_valid(form)

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields['data_acquisto'].widget = DateInput(attrs={'type': 'date'})
        return form


@method_decorator(login_required, name='dispatch')
class SpesaDeleteView(DeleteView):
    model = Spesa
    success_url = reverse_lazy("spese:all-list")
